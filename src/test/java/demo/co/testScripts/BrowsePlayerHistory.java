package demo.co.testScripts;

import org.testng.annotations.Test;

import demo.co.pageObjects.*;
import demo.co.Base.*;

public class BrowsePlayerHistory extends BaseClass {

	// Calling base class constructor.
	public BrowsePlayerHistory() {
		super();
	}

	@Test
	public void Player() throws Exception {

		PlayerSearch("Sir Donald Bradman");
		PlayerSearch("Virat Kohli");
		PlayerSearch("Shane Warne");

	}

	public void PlayerSearch(String PlayerName) throws InterruptedException {

		Object_BrowsePlayerHistory main = new Object_BrowsePlayerHistory();

		main.tapMore.click();
		main.tapMore.click();
		Thread.sleep(2000);

		main.tapBrowsePlayer.click();
		Thread.sleep(1000);

		main.tapSearch.sendKeys(PlayerName);
		Thread.sleep(3000);

		main.tapSearchSuggestion.click();
		Thread.sleep(2000);
		
		String title = main.getTitle.getText();
		String bornDate = main.getBornDate.getText();
		String bornPlace = main.getBirthPlace.getText();
		String nickname = main.getNickName.getText();
		String height = main.getHeight.getText();
		String role = main.getRole.getText();
		String battingStyle = main.getBattingStyle.getText();
		String bowlingStyle = main.getBowlingStyle.getText();
		
		System.out.println("Player name = "+title);
		System.out.println("Born Date = "+bornDate);
		System.out.println("Born Place = "+bornPlace);
		System.out.println("Nickname = "+nickname);
		System.out.println("Height = "+height);
		System.out.println("Role = "+role);
		System.out.println("Batting Style = "+battingStyle);
		System.out.println("Bowling Style = "+bowlingStyle);
		System.out.println("\n\n");
		
		main.tapBack.click();
		Thread.sleep(2000);
		
		main.tapBack.click();
		Thread.sleep(2000);
		
		
		

		/*
		 * main.tapMenRankings.click(); Thread.sleep(3000);
		 * 
		 * main.tapTeams.click(); Thread.sleep(3000);
		 * 
		 * main.tapTest.click(); Thread.sleep(3000);
		 * 
		 * String TestRank = main.fetchIndiaTestRank.getText();
		 * System.out.println("India Men Test Rank is No. "+TestRank);
		 * 
		 * main.tapOdi.click(); Thread.sleep(3000);
		 * 
		 * String OdiRank = main.fetchIndiaOdiRank.getText();
		 * System.out.println("India Men ODI Rank is No. "+OdiRank);
		 * 
		 * 
		 * main.tapT20.click(); Thread.sleep(3000);
		 * 
		 * String T20Rank = main.fetchIndiaT20Rank.getText();
		 * System.out.println("India Men T20 Rank is No. "+T20Rank);
		 */

	}

}