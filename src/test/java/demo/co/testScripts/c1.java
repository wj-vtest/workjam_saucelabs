package demo.co.testScripts;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.agiletestware.pangolin.annotations.Pangolin;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.google.common.base.Function;

import demo.co.Base.BaseClass;
import demo.co.Base.CommonFunctions;
import demo.co.pageObjects.HomePage;
import demo.co.pageObjects.LoginPage;
import demo.co.pageObjects.TimeAttendance;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

/**
 * @author Bobby
 */
@Pangolin(sectionPath = "Master\\Section\\WebDriver1")
public class c1 extends BaseClass {

	public c1()
	{
		super();
	}

	@Test
	public void c1() throws FileNotFoundException, InterruptedException, IOException {
		LoginPage loginPage = new LoginPage();
		HomePage homePage = new HomePage();
		TimeAttendance t = new TimeAttendance();
		
		TimeZone timeZone = TimeZone.getTimeZone("Canada/Eastern");
		DateFormat dateFormat = new SimpleDateFormat("h:mm a");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Canada/Eastern"));
		
		common(loginPage.textBoxUserName);
		loginPage.textBoxUserName.sendKeys(prop.getProperty("Employee1UserName"));
		common(loginPage.buttonContinue);
		loginPage.buttonContinue.click();
		common(loginPage.textBoxPassword);
		loginPage.textBoxPassword.sendKeys(prop.getProperty("Employee1Password"));
		common(loginPage.buttonLogin);
		loginPage.buttonLogin.click();
		common(homePage.header);
		common(homePage.buttonSideMenu);
		homePage.buttonSideMenu.click();
		common(homePage.TimenAttendance);
		homePage.TimenAttendance.click();
		
		common(t.clockin);
		t.clockin.click();
		t.punch.click();
		String clockintime=dateFormat.format(new Date());
		common(t.popup);
		
		common(t.breakin);
		t.breakin.click();
		t.punch.click();
		String breakintime=dateFormat.format(new Date());
		common(t.popup);
		
		common(t.breakout);
		t.breakout.click();
		t.punch.click();
		String breakouttime=dateFormat.format(new Date());
		common(t.popup);

		common(t.mealin);
		t.mealin.click();
		t.punch.click();
		String mealintime=dateFormat.format(new Date());
		common(t.popup);
		
		common(t.mealout);
		t.mealout.click();
		t.punch.click();
		String mealouttime=dateFormat.format(new Date());
		common(t.popup);
		
		common(t.clockout);
		t.clockout.click();
		t.punch.click();
		String clockouttime=dateFormat.format(new Date());
		common(t.popup);

		common(t.timecards);
		t.timecards.click();

		
		List<WebElement> weekDateCards = t.currentDateRange;		
		String week = CommonFunctions.getCurrentWeek(getDriver());
		String date = CommonFunctions.getCurrentDate(getDriver());
//		System.out.println((weekDateCards.get(weekDateCards.size()-1).getText())+" :out: "+week);
		Assert.assertEquals((weekDateCards.get(weekDateCards.size()-1).getText()).contains(week), true,"current week not found"); 
		weekDateCards.get(weekDateCards.size()-1).click();
		CommonFunctions.scrollToElementUsingText(getDriver(), CommonFunctions.getCurrentDatePlus2Date(getDriver()));
//		List<MobileElement> currentDateCards=t.getCurrentDate();
//		List<MobileElement> timeLogs=t.getTimeLogs();
//		//System.out.println("length:"+(currentDateCards.get(currentDateCards.size()-1).getText())+" date:"+date);
//		Assert.assertEquals((currentDateCards.get(currentDateCards.size()-1).getText()).contains(date), true,"current day not found"); 		
//   		
//  		
//    		Assert.assertEquals((timeLogs.get(timeLogs.size()-1).getText()), clockouttime,"Not found clockout time");
//    		Assert.assertEquals((timeLogs.get(timeLogs.size()-2).getText()), mealouttime,"Not found mealout time");	
//    		Assert.assertEquals((timeLogs.get(timeLogs.size()-3).getText()), mealintime,"Not found mealin time");
//    		Assert.assertEquals((timeLogs.get(timeLogs.size()-4).getText()), breakouttime,"Not found breakout time");
//    		Assert.assertEquals((timeLogs.get(timeLogs.size()-5).getText()), breakintime,"Not found breakin time");
//    		Assert.assertEquals((timeLogs.get(timeLogs.size()-6).getText()), clockintime,"Not found clockin time");

    	
    		
    		
		

	}
public void common(final WebElement currentDateRange)
{
	Wait<WebDriver> wait = new FluentWait<WebDriver>(getDriver())
		    .withTimeout(30, TimeUnit.SECONDS)
		    .pollingEvery(5, TimeUnit.SECONDS)
		    .ignoring(NoSuchElementException.class);

		WebElement foo = wait.until(new Function<WebDriver, WebElement>() 
		{
		  public WebElement apply(WebDriver driver) {
		  return currentDateRange;
				  }
		});
}

}
