package demo.co.testScripts;

import java.awt.Window;
import java.io.FileNotFoundException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import demo.co.Base.BaseClass;
import demo.co.Base.CommonFunctions;
import demo.co.pageObjects.SurveyPage;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.github.bonigarcia.wdm.WebDriverManager;

public class C2_Survey extends BaseClass {
	
	WebDriver driver1;
	@Test
	public void C2_Survey() throws InterruptedException, FileNotFoundException
	{
		//logger = extentReports.createTest("Admin can create survey and Employee can finish survey and earn Badge");		
		WebDriverManager.chromedriver().setup();
        driver1 =new ChromeDriver();
        driver1.manage().window().maximize();
		driver1.get("https://qa-app.workjamnp.com/");
//		 WebDriverWait wait = new WebDriverWait(driver1,30);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='username']"))).sendKeys("vtest-poc.admin@vtest.poc");
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[text()='Continue']"))).click();
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("password"))).sendKeys("789789Aa");
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[text()='Log In']"))).click();
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Company details']")));
//		WebElement element = driver1.findElement(By.xpath("//a[@href='#/admin/manage-company/companies/1576540676721/surveys']//child::span"));
//		
//		((JavascriptExecutor)driver1).executeScript("window.scroll(0,'"+(element.getLocation().getY()-100)+"')");
//		
//		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@href='#/admin/manage-company/companies/1576540676721/surveys']//child::span")));
//		//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
//		Thread.sleep(2000);
//		element.click();
//		//driver.findElement(By.xpath("//a[@href='#/admin/manage-company/companies/1576540676721/surveys']//child::span")).click();
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[text()='Add survey']"))).click();
//		WebElement element1 = driver1.findElement(By.xpath("//div[5]//div[1]//input[1]"));
//		((JavascriptExecutor)driver1).executeScript("window.scroll(0,'"+(element1.getLocation().getY()-200)+"')");
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[5]//div[1]//input[1]")));
//		String a="Survey_"+CommonFunctions.generateRandomString(4);
//		  element1.sendKeys(a);
//		   Thread.sleep(1000);
//		   WebElement element2 = driver1.findElement(By.xpath("//select[@ng-model='surveyCtrl.survey.type']"));
//		   ((JavascriptExecutor)driver1).executeScript("window.scroll(0,'"+(element2.getLocation().getY()-200)+"')");
//		   wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@ng-model='surveyCtrl.survey.type']")));
//			   element2.click();
//			   Select drpdwnSurveyType = new Select(driver1.findElement(By.xpath("//select[@ng-model='surveyCtrl.survey.type']")));
//			   drpdwnSurveyType.selectByIndex(2);
//			   SurveyPage surveyPage= new SurveyPage(driver1);
//			   surveyPage.getSurveyBadges().sendKeys("new_badge");
//			   Thread.sleep(5000);
//			   driver1.findElement(By.xpath("//a[@title='new_badge']")).click();
//			 //a[contains(text(),'w_badge')]
//			   Select drpdwnTargetAudience = new Select(driver1.findElement(By.id("segmentationList")));
//			   drpdwnTargetAudience.selectByVisibleText("All");
//			   WebElement element4 = driver1.findElement(By.xpath("//input[@id='surveyStatus']//parent::label"));
//			   ((JavascriptExecutor)driver1).executeScript("window.scroll(0,'"+(element4.getLocation().getY()-200)+"')");
//			  // driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//			  wait.until(ExpectedConditions.visibilityOf(element4));
//				   element4.click();
//			   WebElement element3 = driver1.findElement(By.xpath("//span[text()='Add question']"));
//			   ((JavascriptExecutor)driver1).executeScript("window.scroll(0,'"+(element3.getLocation().getY()-200)+"')");
//			   wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Add question']")));
//				   element3.click();
//				   Select qtype = new Select(driver1.findElement(By.xpath("//select[@id='questionType']")));
//				   qtype.selectByIndex(0);
//				   driver1.findElement(By.xpath("//div[@class='row form-group ng-scope']//input[@id='question-']")).sendKeys("What is Company working hours?");
//				   driver1.findElement(By.xpath("//input[@name='answer-0-en']")).sendKeys("7");
//				   driver1.findElement(By.xpath("//div[@class='panel-body']//div[2]//div[5]//div[1]//div[2]//div[4]//input[1]")).sendKeys("0");
//				   driver1.findElement(By.xpath("//div[@class='row form-group ng-scope']//*[@id='plus-circle-icon']//*[contains(@class,'icon')]")).click();
//				   driver1.findElement(By.xpath("//input[@name='answer-1-en']")).sendKeys("9");
//				   driver1.findElement(By.xpath("//body[@class='no-scroll']//div[@id='app']//div[@class='ng-scope']//div[@class='ng-scope']//div[@class='ng-scope']//div[@class='ng-scope']//div[3]//div[5]//div[1]//div[2]//div[4]//input[1]")).sendKeys("10");
//				   driver1.findElement(By.xpath("//div[@class='form-action-buttons']//input[@class='btn btn-primary pull-right ng-scope']"));
//				   WebElement element7 = driver1.findElement(By.xpath("//div[@class='form-action-buttons']//input[@class='btn btn-primary pull-right ng-scope']"));
//					((JavascriptExecutor)driver1).executeScript("window.scroll(0,'"+(element7.getLocation().getY())+"')");
//					Thread.sleep(5000);
//					
//					element7.click();
//					 WebElement element8 = driver1.findElement(By.xpath("//input[@class='btn btn-primary pull-right ng-scope']"));
//					((JavascriptExecutor)driver1).executeScript("window.scroll(0,'"+(element8.getLocation().getY()-200)+"')");
//					Thread.sleep(5000);
//					element8.click();
//		Properties property = CommonFunctions.getProperty(Constants.CREDENTIAL_FILE_PATH);
//		LoginPage l = new LoginPage(driver);
//		l.getLoginFunctionality(property.getProperty("Employee1UserName"), property.getProperty("Employee1Password"));
//		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		((AndroidDriver<MobileElement>)driver).findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().text(\""+"Surveys"+"\"))").click();
//		((AndroidDriver<MobileElement>)driver).findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().text(\""+a+"\"))").click();		
//		driver.findElement(By.id("com.android.packageinstaller:id/permission_deny_button")).click();
//		driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[2]/androidx.viewpager.widget.ViewPager/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[3]/android.widget.ImageView")).click();
//		driver.findElement(By.id("com.workjam.workjam.qa:id/view_pager_next_button")).click();
		
	}

}
