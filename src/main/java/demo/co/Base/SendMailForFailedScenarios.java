package demo.co.Base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class SendMailForFailedScenarios {

	public static Properties prop = new Properties();

	public static void SendMail(String newFolderName,String status) throws IOException {

		FileInputStream ip = new FileInputStream("src//main//java//demo//co//Config//config.properties");
		prop.load(ip);

		// Recipient's email ID needs to be mentioned.
		final String username = prop.getProperty("FromEmailUsername");
		final String password = prop.getProperty("EmailPassword");

		System.out.println(username);
		System.out.println(password);
		
		Properties props = new Properties();
		props.put("mail.smtp.auth", true);
		props.put("mail.smtp.starttls.enable", true);
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			String email = prop.getProperty("ToEmailUsername");
			String subject = "Attention!! Failed Reports";

			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);
			// Set From: header field of the header.
			message.setFrom(new InternetAddress(username));
			// Set To: header field of the header.
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));

			// Set Subject: header field
			message.setSubject(subject);

			// Create the message part
			BodyPart messageBodyPart = new MimeBodyPart();

			// Fill the message
			messageBodyPart.setText("Hello Sir/Madam"
					+ ",\n\n Hope you are doing good.\n\nTest status -"
					+" \n | No | PackageName.className.TestCaseName | status |\n"
					+status+" Attached is the screenshots for the failed scenarios.\n\n For further details, contact the QA team.\n\n"
					+ "\n\n Regards" + "\n QA Team");

			// Create a multipar message
			Multipart multipart = new MimeMultipart();

			// Set text message part
			multipart.addBodyPart(messageBodyPart);

			File directory = new File("screenshot\\" + newFolderName);
			Path dir = directory.toPath();
			DirectoryStream<Path> stream = Files.newDirectoryStream(dir);
			for (Path file : stream) {
				if (file.getFileName().toString().contains("png") && file.getFileName().toString().contains("fail"))
				{
					addAttachment(multipart, "screenshot\\" + newFolderName + "\\" + file.getFileName());
				}
			}
			System.out.println("after status");
			// Send the complete message parts
			message.setContent(multipart);

			// Send message
			Transport.send(message);
			System.out.println("Sent message successfully to " + email);

		} catch (Exception mex) {
			// mex.printStackTrace();
		}
	}

	private static void addAttachment(Multipart multipart, String filename) throws MessagingException {
		DataSource source = new FileDataSource(filename);
		BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName(new File(filename).getName());
		multipart.addBodyPart(messageBodyPart);
	}
}
